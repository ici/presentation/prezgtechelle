\documentclass[xcolor=table]{beamer}
\usetheme[progressbar=frametitle, sectionpage=none]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage{amssymb, amsmath}
\usepackage[absolute,overlay]{textpos}
\usepackage{booktabs}
\usepackage{pgf, tikz}
\usepackage{pdfcomment}
\usepackage{pgfplots}
\usepackage{algpseudocode}
\usepackage{algorithm}
\usepackage{hyperref}
\usepackage[utf8]{inputenc}
\usepackage{xspace}
\usepackage{color}
\usepackage{animate}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand\FontImportant{\fontsize{15}{20}\selectfont}
\newcommand\FontMoyenImportant{\fontsize{14}{17}\selectfont}
\newcommand\FontPetit{\fontsize{8}{6}\selectfont}
\definecolor{links}{HTML}{2A1B81}
\hypersetup{colorlinks,linkcolor=,urlcolor=links}
\definecolor{green}{cmyk}{1,0,1,0.12}

\title{Projet ICI — Présentation Groupe de Travail Échelles}
\author{Maxime Colomb, Carl Graham, Denis Talay}

\titlegraphic{\vspace{-0.cm}\includegraphics[height=1.1cm]{img/logoIGN.png}\hfill\includegraphics[height=1.1cm]{img/inr_logo_noir.png}}
\date{14 Novembre 2023}

\makeatletter
\newcommand\addsectiontotoc[1]{%
  \addtocontents{toc}{%
    \protect\beamer@sectionintoc{\the\c@section}{#1}{\the\c@page}{\the\c@part}%
                                {\the\beamer@tocsectionnumber}}
}
\makeatother
\newcommand{\MYhref}[3][blue]{\href{#2}{\color{#1}{#3}}}%\right) 
\makeatletter
\newcommand{\miniscule}{\@setfontsize\miniscule{4}{5}}% \tiny: 5/6
\makeatother

\setbeamerfont*{section in head/foot}{size=\miniscule}
\setbeamercolor{section in head/foot}{parent=palette primary}
\setbeamerfont{title}{size=\large}
\setbeamerfont{date}{size=\scriptsize}

\addtobeamertemplate{frametitle}{}{%
  \begin{textblock*}{5cm}(11.3cm,0.03cm)
    \insertsectionnavigation{4cm}
  \end{textblock*}
}


\defbeamertemplate{section page}{progressbarcomplete}{
  \centering
  \begin{minipage}{22em}
    \raggedright
    \usebeamercolor[fg]{section title}
    \usebeamerfont{section title}
    \insertsection\\[-1ex]
    \usebeamertemplate*{progress bar in section page}
    \par
    \ifx\insertsubsection\@empty\else%
    \usebeamercolor[fg]{subsection title}%
    \usebeamerfont{subsection title}%
    \insertsubsection
    \fi
  \end{minipage}
  \par
  \vspace{\baselineskip}
}
\begin{document}

\maketitle

\begin{frame}{Introduction - Objectifs}
	
	\textbf{ICI~:} initiative originale d\'emarr\'ee en octobre~2020 dont 
	nous ne connaissons pas d'\'equivalent. 
	
	\vspace{0.2cm}
	\textbf{Son but~:} permettre aux pouvoirs publics et aux 
	\textbf{\'epid\'emiologistes} de quantifier \`a partir de \textbf{simulations}
	l'impact futur de diverses \textbf{mesures sanitaires} envisag\'ees 
	sur l'\'evolution d'une \'epid\'emie (par exemple, confinement total ou 
	partiel, fermeture des \'ecoles, politiques de tests).
	
	\vspace{0.2cm}
	\textbf{ICI} est un simulateur de propagation d’épidémies 
	\textbf{à l'échelle individuelle} reposant sur une \textbf{modélisation 
		très détaillée} d’un espace urbain et de la circulation des 
	personnes \`a l’int\'erieur de cet espace.
	
	\vfill
\end{frame}

\begin{frame}{Introduction - Objectifs}	
	En particulier, pour l'espace urbain mod\'elis\'e et pour la population 
	synth\'etique simul\'ee, ICI permet de calculer par m\'ethode
	de Monte Carlo \`a tr\`es grande \'echelle, 
	des \textbf{informations statistiques} sur
	\begin{itemize} 
		\small
		\item la dynamique du nombre de personnes infect\'ees selon divers 
		crit\`eres tels que la classe d'\^age, les comorbidit\'es, les 
		quartiers de r\'esidence, les lieux de travail~;
		\item le degr\'e de saturation \`a venir d'\'etablissements 
		hospitaliers selon les mesures sanitaires prises.
	\end{itemize}
	
	\vspace{0.2cm}
	ICI a vocation \`a traiter des \textbf{\'epid\'emies diverses} et est 
	construit de mani\`ere modulaire. 
	
	\vspace{0.2cm}
	Le mod\`ele ICI repose sur le \textbf{couplage} de plusieurs types de
	mod\'elisation, de m\'ethodes de fusion de \textbf{donn\'ees h\'et\'erog\`enes} 
	massives, et de techniques de pointe de \textbf{calcul scientifique}.
\end{frame}



\begin{frame}{Exemples de résultats possibles}
	\only<1>{\includegraphics[width=1.1\textwidth]{./img/scenario1.png}}%
	\only<2>{\includegraphics[width=1.1\textwidth]{./img/scenario2.png}}%
\end{frame}

\begin{frame}{Architecture du simulateur ICI}
	\textbf{ \large ICI est composé de plusieurs modules :}
	\begin{block}{I) Le générateur de données}
		\vfill
		\begin{enumerate}
			\item Une modélisation \textbf{exhaustive} et très précise du tissu urbain et de l'\textbf{intérieur des bâtiments} étiquetés avec les \textbf{usages} (logements, lieux de travail, POI).
			\item Une \textbf{population} synthétique répartie dans des ménages structurés.
			\item La \textbf{circulation} des individus entre leurs différentes activités dans cet espace urbain.
		\end{enumerate}
	\end{block}
	\vfill
	\uncover<2->{\begin{block}{II) Le simulateur de diffusion épidémique}
	\end{block}}
	\uncover<3->{\begin{block}{III) La mise à disposition des résultats de simulation}
	\end{block}}
\end{frame}



\section{Modélisation urbaine}
\subsection{Recensement, fusion et enrichissement des activités}
\begin{frame}{}
	\sectionpage
\end{frame}

\begin{frame}{Modélisation spatiale de l'intérieur des bâtiments}
	Le \textbf{bâtiment} est le centre de notre modélisation. 
	\vfill
	Chaque bâtiment peut héberger des éléments parmi les suivants :
	\vfill
	\begin{block}{Lieux de travail}
		\vfill
		Activité professionnelle, classe, étude supérieures, etc.
	\end{block}
	\begin{block}{Points d'intérêt (POI)}
		\vfill
		Commerces, loisirs, démarches, etc.
	\end{block}
	\begin{block}{Logements}
		\vfill
		Vides ou occupés par un ménage.
	\end{block}
\end{frame}

\begin{frame}{Lieux de travail}
	Recensement des activités économiques dans la base de données \textbf{SIRENE}. 
	\vfill
	Effectifs et nombre de classes des écoles.
	\vfill 
	Effectifs des établissements supérieurs.
	\vfill
	\begin{block}{Exemple sur le Vème arrondissement de Paris}
		\includegraphics[width=0.715\textwidth]{./img/wp.png}
	\end{block}
\end{frame}

\begin{frame}{POI : Fusion de données de références}
	\includegraphics[width=\textwidth]{./img/schemaAmeniteSource}
	\vfill
	Création d'une \textbf{nomenclature} spéciale comportant 34 classes et adaptée aux objectifs d'ICI. %et adaptée aux décrets de fermeture des confinements.
	\vfill
	\textbf{Fusion} automatisée et enrichissement des données. 
\end{frame}

\section{Population Synthétique}
\subsection{Génération de ménages structurés}
\begin{frame}{}
	\sectionpage
\end{frame}

\begin{frame}{Population synthétique quadri-couche}
		\begin{columns}
			\begin{column}{0.45\textwidth}
				\textbf{Un générateur original}
				\begin{itemize}
					\item Fortement \textbf{géolocalisé}.
					\item Configurations \textbf{hiérarchisées}, plausibles et respectant des contraintes méso ou globales.
					\item \textbf{Flexible} aux données disponibles et à l'ajout de nouveaux attributs.
				\end{itemize}
				\vfill 
				Méthode de \textbf{rejet stochastique sous contrainte}.
			\end{column}
			\begin{column}{0.55\textwidth}
				\begin{center}
					\centering\includegraphics[height=0.7\textheight]{./img/popGen.png}	
				\end{center}
			\end{column}

		\end{columns}
\end{frame}

\begin{frame}{Résultats de population synthétique}
	\only<1>{\centering\includegraphics[height=0.7\textheight]{./img/indivStock.png}}
	\only<2>{\centering\includegraphics[width=1.1\textwidth]{./img/outMacro2.png}}
	\only<3>{\centering\includegraphics[width=0.9\textwidth]{./img/outMacro.png}}
\end{frame}

\section{Circulation des individus entre leurs activités}
\subsection{Qui fait quoi, où et quand}
\begin{frame}
	\sectionpage
\end{frame}

\begin{frame}{Flux de population}
	Pour l'instant, on ne modélise que les \textbf{localisations heure par heure} des individus plutôt que les déplacements précis.
	\vfill
	Fusion de données hétérogènes :
	\begin{itemize}
		\item Profils et raisons de présences des individus grâce à l'\textbf{EGT}.
		\item Localisation fine des présences grâce à des traces de \textbf{données mobiles}.
		\item Flux pendulaires et occasionnels grâce aux fréquentations de la RATP.
	\end{itemize}
	\vfill
	Attribution \textbf{stochastique} des activités individuelles.
\end{frame}

\begin{frame}{Génération d'un agenda}
	%\centering\textbf{Chaîne de Markov}\vfill
	%	\includegraphics[width=\textwidth]{./img/MCComplet.png}\\
	%\textit{paramétrage particulier à partir de l'EGT}}
	\begin{columns}
		\begin{column}{0.35\textwidth}
			\begin{itemize}
				\item Une chaîne de Markov pour le passage d'une activité à une autre. 
				\item Paramétrage pour différent profils d'individus, de temporalité et de localisation, grâce à l'\textbf{EGT}.
			\end{itemize}
		\end{column}
		\begin{column}{0.65\textwidth}
			\includegraphics[width=\textwidth]{./img/presence.png}\\
		\end{column}
	\end{columns}
\end{frame}

\section{Simulation de diffusion épidémique}
\subsection{}
\begin{frame}{}
	\sectionpage
\end{frame}


\begin{frame}{Un modèle à 9 états}
	\begin{columns}
		\begin{column}{0.65\textwidth}
			\includegraphics[height=0.8\textheight]{img/statEpidemio.png}
		\end{column}
		\begin{column}{0.35\textwidth}
			\small
			Probabilités \textbf{paramétrables} et adaptées aux \textbf{caractéristiques des individus} pour passer d'un état à l'autre.
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Loi de contamination}
	\textit{En cas de co-présence avec un individu infectieux, la probabilité de devenir contaminé est :}
	\begin{equation}
		1-\left(1-\frac{coefficient_{transmission}}{distance^{2}}\right)^{duree\_de\_contact * cst}
	\end{equation}
	\vfill
	Dépendant de :
	\begin{itemize}
		\small
		\item Durée de contact entre les individus
		\item Distance/densité entre les individus
		\item Coefficient de transmission (score différent pour chaque individu et lieu)	
	\end{itemize}
	\vfill
	{\small(1) D'après \textit{Hoertel et al., 2020}}
	%très peu de modèles, de données, 
\end{frame}

\begin{frame}{Implémentation de scénarios sanitaires}
	\begin{block}{Multiples scénarios, avec possibilité de les combiner}
		\begin{itemize}
			\item Restrictions de déplacements (couvre-feu, confinement).
			\item Fermeture ciblée de magasins, d'écoles, de travail.
			\item Confinements d'individus infectés.
			\item Stratégies de test.
			\item Masques, distanciation sociale.
			\item Campagnes de vaccination.
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Application à Paris}
	Simulation pour l'instant appliquée aux arrondissements de Paris.
	\vfill
	Technologies de calcul haute performance utilisée pour répliquer un très grand nombre de fois les simulations. 
	\vfill
	\includegraphics[height=0.7\textheight]{./img/res.png}
\end{frame}

\section{Application web du projet ICI}
\subsection{Descriptions et mise à disposition des résultats de simulation}
\begin{frame}{}
	\sectionpage
\end{frame}


\begin{frame}{Descriptions des simulateurs}
	\only<1>{	
		\begin{block}{Détail sur les méthodes utilisées}
			\vfill
			\centering\includegraphics[height=0.7\textheight]{./img/siteWeb.png}	
	\end{block}}
	\only<2>{
	\begin{block}{Visualisation des données géographiques}
		\vfill
		Disponibles sur tout Paris ici : \url{https://ici.saclay.inria.fr/generic_arr.html/}
		\vfill
		\centering\includegraphics[height=0.7\textheight]{./img/dataSaintVic.png}	
	\end{block}}
\end{frame}

\begin{frame}{Application d'exploration des résultats de simulation}
	\href{https://ici.saclay.inria.fr/dist/}{\textit{démo en direct}}
\end{frame}

\section{Conclusions et perspectives}
\begin{frame}{}
	\sectionpage
\end{frame}

\begin{frame}{Un modèle modulaire de génération de données}
	\begin{block}{Un modèle de données générique et interopérable}
		\vfill
		Trois modules \textit{relativement} indépendants.
		\vfill
		Flexibilité quant au besoin de données d'entrée.
	\end{block}
	\vfill
	\begin{block}{Diffusion des données géographiques}
		\vfill
		Génération d'un graph suivant le formalisme RDF et diffusion dans le web sémantique. 
		\vfill 
		Échanges avec divers modèles géographiques.
		\end{block}
\end{frame}

\begin{frame}{Un modèle de propagation d'épidémie}
	\begin{block}{Modèle de diffusion fonctionnel}
		\vfill
		Possibilité d'amélioration de certains sous systèmes (EHPAD, hôpitaux, politiques de tests).
	\end{block}
	\vfill
	\begin{block}{Application à d'autres épidémies}
		\vfill
		Pour l'instant, applicable à toute maladie avec contaminations essentiellement à l’intérieur. 
		\vfill
		Application prévue à d'autres épidémies (transmission par vecteur).
	\end{block}
	\begin{block}{Études de varabilité menées sur plateforme OpenMOLE}
		\begin{itemize}
			\item Études de sensibilité.
			\item Optimisation de politiques sanitaires.
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Collaborations}
	\only<1>{\begin{block}{}
		\begin{itemize}
			\item Intégration de \textbf{comportements différenciés} selon les règles sanitaires (projet SafeCityMap (INSA Lyon)).
			\item \textbf{Visualisation} des résultats avec les indicateurs de LASTIG-Geovis.
			\item Collaboration avec l'INSERM sur les données et les applications épidémiologiques.
			\item Insertion de certains modules dans un catalogue de \textbf{jumeaux numériques} nationaux et souverains.
		\end{itemize}
	\end{block}}
	\only<2>{Collaborateurs :
		\begin{itemize}
			\item \textbf{Maxime Colomb :} INRIA - IGN
			\item \textbf{Denis Talay :} Coordinateur du projet, INRIA
			\item \textbf{Julien Perret :} IGN
			\item \textbf{Carl Graham :} CNRS et Ecole Polytechnique
			\item \textbf{Josselin Garnier :} \'Ecole Polytechnique.
			\item \textbf{Quentin Cormier :} INRIA
			\item \textbf{Aline Carneiro Viana, Razvan Stanica :}
			respectivement INRIA et INSA-Lyon
			\item \textbf{Milica Tomasevic :}
			CNRS et Ecole Polytechnique
			\item \textbf{Laura Grigori :} INRIA
			\item \textbf{Nicolas Gilet :} CEA
	\end{itemize}}
\end{frame}

\begin{frame}[standout]
	Merci de votre attention 
	\vfill
	\renewcommand\UrlFont{\color{white}\itshape}
	{\textcolor{white}{\url{https://ici.saclay.inria.fr}}}
\end{frame}

\end{document}